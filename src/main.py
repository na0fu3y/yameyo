from flask import Flask, json, request, Response, render_template
import sqlite3
import pandas as pd


app = Flask(__name__)
con = sqlite3.connect(database='messages.db', check_same_thread=False)


def create_json_response(obj):
    return Response(response=json.dumps(obj, ensure_ascii=False, indent=4), mimetype='application/json')


@app.route('/index', methods=['GET'])
def index():
    return render_template('index.html')


@app.route('/api/v1/status', methods=['GET'])
def get_status():
    sql = "select julianday(julianday(min(created_at)) + sum(score) - julianday('now')) from MESSAGES"
    data = con.execute(sql).fetchone()
    return create_json_response({'data': {
        'remaining_days': data[0]
    }})


@app.route('/api/v1/messages', methods=['GET'])
def get_messages():
    sql = '''
select M.MESSAGE_ID, M.SCORE, M.REASON_ID, R.REASON, M.CREATED_AT, M.MEMO
from MESSAGES as M
left outer join REASONS as R
on M.REASON_ID = R.REASON_ID'''

    return create_json_response({'data': pd.read_sql_query(sql, con).to_dict(orient='records')})


@app.route('/api/v1/messages/<int:message_id>', methods=['GET'])
def get_message(message_id: int):
    sql = '''
select M.MESSAGE_ID, M.SCORE, M.REASON_ID, R.REASON, M.CREATED_AT, M.MEMO
from MESSAGES as M
left outer join REASONS as R
on M.REASON_ID = R.REASON_ID
where M.MESSAGE_ID = :message_id'''

    data = pd.read_sql_query(sql, con, params={'message_id': message_id}).to_dict(orient='records')

    return create_json_response({'data': data[0] if data else None})


@app.route('/api/v1/messages', methods=['POST'])
def post_message():
    params = {
        'score': request.form.get('score', type=int),
        'reason_id': request.form.get('reason_id', type=int),
        'memo': request.form.get('memo')
    }
    identity = con.execute('insert into MESSAGES(SCORE, REASON_ID, MEMO) values(:score, :reason_id, :memo)', params).lastrowid
    con.commit()

    return create_json_response({'data': identity})


@app.route('/api/v1/reasons', methods=['GET'])
def get_reasons():
    sql = '''
select R.REASON_ID, R.REASON
from REASONS as R
order by (
select sum(M.MESSAGE_ID)
from MESSAGES as M
where R.REASON_ID = M.REASON_ID
) desc'''

    return create_json_response({'data': pd.read_sql_query(sql, con).to_dict(orient='records')})


@app.route('/api/v1/reasons/<int:reason_id>', methods=['GET'])
def get_reason(reason_id: int):
    sql = '''
select R.REASON_ID, R.REASON
from REASONS as R
where R.REASON_ID = :reason_id'''

    data = pd.read_sql_query(sql, con, params={'reason_id': reason_id}).to_dict(orient='records')

    return create_json_response({'data': data[0] if data else None})


@app.route('/api/v1/reasons', methods=['POST'])
def post_reasons():
    params = {
        'reason': request.form.get('reason')
    }
    identity = con.execute('insert into REASONS(REASON) values(:reason)', params).lastrowid
    con.commit()

    return create_json_response({'data': identity})


if __name__ == '__main__':
    con.execute('''
create table if not exists MESSAGES(
MESSAGE_ID integer primary key,
SCORE integer not null,
REASON_ID integer not null,
MEMO text not null,
CREATED_AT text not null default current_timestamp,
foreign key (REASON_ID) references REASONS(REASON_ID))
''')
    con.execute('''
create table if not exists REASONS(
REASON_ID integer primary key,
REASON text not null)
''')
    app.run(host='0.0.0.0', threaded=True)
